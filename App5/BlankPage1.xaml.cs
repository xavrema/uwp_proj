﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace App5
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    /// 
    
    public sealed partial class BlankPage1 : Page
    {
        private Account _currentAccount;
        private Random _randomizer;
        private string[] _allAccount;
        private List<Player> _allPlayers;
        private PassedParameters _passedParam;
        public Account CurrentAccount
        {
            get { return _currentAccount; }
            set { _currentAccount = value; }
        }
        public BlankPage1()
        {
            this.InitializeComponent();
            _randomizer = new Random();

        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter is PassedParameters)
            {
                _passedParam = e.Parameter as PassedParameters;
                CurrentAccount = _passedParam._current;
                _allAccount = _passedParam._txtAccounts;
                _allPlayers = _passedParam._playersAll;
            }
            else {
                CurrentAccount = e.Parameter as Account;
            }
            
            base.OnNavigatedTo(e);
            _txtDisplayMessage.Text = $"Hello {_currentAccount.Username}";
        }
        private void OnClickGoBack(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(MainPage));
        }

        private void OnClickEditSquad(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(BlankPage2), _passedParam);
        }

        private void OnClickSimulate(object sender, RoutedEventArgs e)
        {
            
            int RandomAccount = _randomizer.Next(1,_allAccount.Length-1);
            
            var foundAccount = _allAccount[RandomAccount].Split('|');
            Account Account2 = new Account(Convert.ToInt16(foundAccount[0]), foundAccount[1], foundAccount[2], foundAccount[3], foundAccount[4], foundAccount[5], foundAccount[6]);
            List<Player> team1 = GetTeam(_currentAccount);
            List<Player> team2 = GetTeam(Account2);
            GameEngine Game = new GameEngine(team1,team2);
            var testing = Game.Simulate();
            _txtSimResult.Text = testing;
            
        }

        private List<Player> GetTeam(Account teamAccount) {
            List<Player> Team = new List<Player>();
            Team.Add(_allPlayers[Convert.ToInt16(teamAccount.Squad[0])]);
            Team.Add(_allPlayers[Convert.ToInt16(teamAccount.Squad[1])]);
            Team.Add(_allPlayers[Convert.ToInt16(teamAccount.Squad[2])]);
            Team.Add(_allPlayers[Convert.ToInt16(teamAccount.Squad[3])]);
            Team.Add(_allPlayers[Convert.ToInt16(teamAccount.Squad[4])]);
            return Team;
        }
    }
}
