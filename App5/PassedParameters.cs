﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App5
{
    class PassedParameters
    {
        public Account _current;
        public string[] _txtAccounts;
        public List<Player> _playersAll;

        public PassedParameters(Account passed, string[] accounts, List<Player> allPlayers)
        {

            _current = passed;
            _txtAccounts = accounts;
            _playersAll = allPlayers;
        }
    }
}
