﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App5
{
    class Country
    {
        private string _name;
        private List<Player> _players;

        public string GetName {
            get { return _name; }
        }

        //Would prefer to learn about .net LINQ but only found the resource too late.
        public Country(string Name) {
            _name = Name;
            _players = new List<Player>();
        }

        public void AddPlayer(Player playerIn) {
                _players.Add(playerIn);
        }
    }
}
