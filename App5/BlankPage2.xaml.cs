﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;
using Windows.UI.Xaml.Media.Imaging;
using System.Threading.Tasks;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace App5
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class BlankPage2 : Page
    {
        #region Field Variables
        private Account _currentAccount;
        private List<Player> _allPlayers;
        private Player _playerChosen;
        private PassedParameters _passedParam;

        #endregion
        #region Properties
        public Account CurrentAccount {
            get { return _currentAccount; }
            set { _currentAccount = value; }
        }
        #endregion
        public BlankPage2()
        {
            this.InitializeComponent();

        }
        private void LoadAssets()
        {
            List<string> Countries = new List<string>();
            for (byte i=0;i<_allPlayers.Count;i++) {
                
                {
                    if (!Countries.Contains(_allPlayers[i].GetPlayerCountry))
                    {

                        Countries.Add(_allPlayers[i].GetPlayerCountry);
                        _spanelPlayerFilters.Children.Add(CreateFilterButton($"{_allPlayers[i].GetPlayerCountry}"));
                        _spanelPlayerFilters.Children.Add(CreateFilterStackPanel($"{_allPlayers[i ].GetPlayerCountry}"));
                    }
                }
            }
            if (_currentAccount.Squad[0] != "000")
            {
                _btnAttackerLeft.CommandParameter = _allPlayers[Convert.ToInt16(_currentAccount.Squad[0]) - 1];
                _btnAttackerLeft.Content = _allPlayers[Convert.ToInt16(_currentAccount.Squad[0]) - 1].GetPlayerName;
                _btnAttackerRight.CommandParameter = _allPlayers[Convert.ToInt16(_currentAccount.Squad[1]) - 1];
                _btnAttackerRight.Content = _allPlayers[Convert.ToInt16(_currentAccount.Squad[1]) - 1].GetPlayerName;
                _btnDefenderLeft.CommandParameter = _allPlayers[Convert.ToInt16(_currentAccount.Squad[2]) - 1];
                _btnDefenderLeft.Content = _allPlayers[Convert.ToInt16(_currentAccount.Squad[2]) - 1].GetPlayerName;
                _btnDefenderRight.CommandParameter = _allPlayers[Convert.ToInt16(_currentAccount.Squad[3]) - 1];
                _btnDefenderRight.Content = _allPlayers[Convert.ToInt16(_currentAccount.Squad[3]) - 1].GetPlayerName;
                _btnGoalKeeper.CommandParameter = _allPlayers[Convert.ToInt16(_currentAccount.Squad[4]) - 1];
                _btnGoalKeeper.Content = _allPlayers[Convert.ToInt16(_currentAccount.Squad[4]) - 1].GetPlayerName;
            }
            else
            {
                _btnAttackerLeft.Content = "Please Choose An Attacker";
                _btnAttackerRight.Content = "Please Choose An Attacker";
                _btnDefenderLeft.Content = "Please Choose An Defender";
                _btnDefenderRight.Content = "Please Choose An Defender";
                _btnGoalKeeper.Content = "Please Choose An Goalkeeper";

            }
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            //CallLocalAssetsPlayers();
            if (e.Parameter is PassedParameters)
            {
                CurrentAccount = (e.Parameter as PassedParameters)._current as Account;
                _allPlayers = (e.Parameter as PassedParameters)._playersAll as List<Player>;
                _passedParam = e.Parameter as PassedParameters;

            }
            else {
                CurrentAccount = e.Parameter as Account;
            }
            if (CurrentAccount.Squad[0] == "000")
            {
                _txtTitleBar.Text = "Create Team";
            }
            else {
                _txtTitleBar.Text = "Edit Team";
            }
            LoadAssets();
            base.OnNavigatedTo(e);
        }
        private async void OnClickGoBack(object sender, RoutedEventArgs e)
        {
            if (_currentAccount.Squad[0] == "000" || _currentAccount.Squad[1] == "000" || _currentAccount.Squad[2] == "000" || _currentAccount.Squad[3] == "000" || _currentAccount.Squad[4] == "000") {

                MessageDialog msg = new MessageDialog("Please Fill all Players");
                await msg.ShowAsync();
                return;
            }
            _passedParam._current = _currentAccount;
            this.Frame.Navigate(typeof(BlankPage1), _passedParam);
        }

        private void OnLoad(object sender, RoutedEventArgs e)
        {
           //Get All Players from the player file, sort them by country, then by position and store those
           //positions in a button click



        }

        private Button CreateFilterButton(string ButtonContent) {
            Button filterCountrybtn = new Button {
                Content = ButtonContent,
                Name = $"{ButtonContent}",
                HorizontalAlignment = HorizontalAlignment.Stretch
                
            };
            filterCountrybtn.Click += new RoutedEventHandler(OnClickShowPosition);
            return filterCountrybtn;
        }
        private void OnClickShowPosition(object sender, RoutedEventArgs e)
        {
            Button btnWhoSentMe = (Button)sender;

            Button filterAttackerbtn = CreateFilterButton("Attackers");
            filterAttackerbtn.Background = new SolidColorBrush(Colors.PaleVioletRed);
            filterAttackerbtn.Click += new RoutedEventHandler(DisplayPosition);
            Button filterDefenderbtn = CreateFilterButton("Defenders");
            filterDefenderbtn.Background = new SolidColorBrush(Colors.CornflowerBlue);
            filterDefenderbtn.Click += new RoutedEventHandler(DisplayPosition);
            Button filterGoalkeeperbtn = CreateFilterButton("Goalkeepers");
            filterGoalkeeperbtn.Background = new SolidColorBrush(Colors.LightGoldenrodYellow);
            filterGoalkeeperbtn.Click += new RoutedEventHandler(DisplayPosition);
            StackPanel AttackerStack = CreateFilterStackPanel("Attacker");
            StackPanel DefenderStack = CreateFilterStackPanel("Defender");
            StackPanel GoalkeeperStack = CreateFilterStackPanel("Goalkeeper");
            for (byte i = 0; (i < _spanelPlayerFilters.Children.Count() - 1); i+=2)
            {
                var guessingWhoSent =_spanelPlayerFilters.Children[i] as Button;
                if (btnWhoSentMe.Name == guessingWhoSent.Name)
                {
                    var newStack = _spanelPlayerFilters.Children[i + 1] as StackPanel;
                    if (newStack.Children[0] is Button)
                    {
                        newStack.Children.Clear();
                    }
                    else
                    {
                        newStack.Children.Add(filterAttackerbtn);
                        newStack.Children.Add(AttackerStack);
                        newStack.Children.Add(filterDefenderbtn);
                        newStack.Children.Add(DefenderStack);
                        newStack.Children.Add(filterGoalkeeperbtn);
                        newStack.Children.Add(GoalkeeperStack);
                    }
                }
            }



        }
        public StackPanel CreateFilterStackPanel(string Stackname) {
            StackPanel filterCountrystack = new StackPanel {
                Name = $"{Stackname}",
            };
            return filterCountrystack;
        }

        private void DisplayPosition(object sender, RoutedEventArgs e) {
            var sentButton = sender as Button;
            var btnParent = sentButton.Parent as StackPanel;

            var Location = btnParent.Children.IndexOf(sender as UIElement);
            var placePlayers = btnParent.Children[Location + 1] as StackPanel;
            if (placePlayers.Children[0] is Button) {
                placePlayers.Children.Clear();
                return;

            }
            List<string> playersStack = AddPlayers(btnParent.Name, sentButton.Name);
            
            for (byte i = 0; i < playersStack.Count; i++) {
                Button btnCreatedPlayer = CreateFilterButton(playersStack[i]);
                btnCreatedPlayer.Background = new SolidColorBrush(Colors.RoyalBlue);
                btnCreatedPlayer.Foreground = new SolidColorBrush(Colors.AntiqueWhite);
                btnCreatedPlayer.Click += new RoutedEventHandler(ShowPlayerStats);
                placePlayers.Children.Add(btnCreatedPlayer);
            }
            
            
        }

        private void ShowPlayerStats(object sender, RoutedEventArgs e)
        {
            var btnSentPlayer = sender as Button;
            for (byte i=0; i<_allPlayers.Count; i++) {

                if (_allPlayers[i].GetPlayerName == btnSentPlayer.Name) {
                    CreatePlayerProfile(_allPlayers[i]);
                    return;
                }

            }
        }

        private void CreatePlayerProfile(Player player)
        {
            StackPanel MainContainer = new StackPanel {
                Width = _col1.ActualWidth*2,
                Height = _row1.ActualHeight 
            };
            


            TextBlock PlayerName = new TextBlock
            {
                Text = player.GetPlayerName,
                FontSize = 12,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            TextBlock PlayerCountry = new TextBlock
            {
                Text = player.GetPlayerCountry,
                FontSize = 12,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            TextBlock PlayerPosition = new TextBlock
            {
                Text = player.GetPlayerPosition,
                FontSize = 12,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            TextBlock PlayerNumber = new TextBlock
            {
                Text = player.GetPlayerNumber,
                FontSize = 12,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            TextBlock PlayerDescription = new TextBlock
            {
                Text = player.GetPlayerDescription,
                FontSize = 12,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            TextBlock PlayerStats = new TextBlock
            {
                Text = string.Join(",", player.GetPlayerStats),
                FontSize = 12,
                HorizontalAlignment = HorizontalAlignment.Left
            };
            Button teamNameSender = new Button
            {
                Content = "Add",
                FontSize = 24,
                Name = player.GetPlayerIdentifier.ToString(),
                CommandParameter = player
            };
            teamNameSender.Click += new RoutedEventHandler(AddPlayerToSquad);
            Image playerProfile = new Image
            {
                Source = new BitmapImage { UriSource = new Uri($"ms-appx:///Assets/Profile.png", UriKind.Absolute) },
                Height = _row1.ActualHeight / 2,
                Width = _col1.ActualWidth /4

            };
            StackPanel PlayerInfoContainer = new StackPanel {
                HorizontalAlignment = HorizontalAlignment.Left
            };
            StackPanel PlayerStatsContainer = new StackPanel {
                HorizontalAlignment = HorizontalAlignment.Left
            };
            StackPanel TopContainer = new StackPanel {
                Orientation = Orientation.Horizontal
            };
            PlayerStatsContainer.Children.Add(PlayerStats);
            PlayerInfoContainer.Children.Add(PlayerName);
            PlayerInfoContainer.Children.Add(PlayerCountry);
            PlayerInfoContainer.Children.Add(PlayerPosition);
            PlayerInfoContainer.Children.Add(PlayerNumber);
            PlayerInfoContainer.Children.Add(PlayerDescription);
            TopContainer.Children.Add(playerProfile);
            TopContainer.Children.Add(PlayerInfoContainer);
            MainContainer.Children.Add(TopContainer);
            MainContainer.Children.Add(PlayerStatsContainer);
            MainContainer.Children.Add(teamNameSender);

            Flyout EnterInfo = new Flyout
            {
            };
            EnterInfo.Content = MainContainer;
            EnterInfo.ShowAt(_spanelFlyout as FrameworkElement);
        }

        private async void AddPlayerToSquad(object sender, RoutedEventArgs e)
        {
            //Take player and put into a postion
            var Player = (sender as Button).CommandParameter as Player;
            MessageDialog msg = new MessageDialog("Please Select Position");
            await msg.ShowAsync();
            _playerChosen = Player;



        }

        private List<string> AddPlayers(string CountryName, string Position)
        {
            List<string> players = new List<string>();
            for (byte i = 0; i < _allPlayers.Count; i++)
            {
                if (_allPlayers[i].GetPlayerCountry == CountryName)
                {
                    if (_allPlayers[i].GetPlayerPosition == Position.Remove(Position.Length - 1))
                    {
                        players.Add(_allPlayers[i].GetPlayerName);
                    }

                }

            }

            return players;
        }
        private async void OnClickPosition(object sender, RoutedEventArgs e)
        {
            var btnClicked = sender as Button;
            if (_playerChosen != null) {
                btnClicked.CommandParameter = _playerChosen;
                btnClicked.Content = _playerChosen.GetPlayerName;
                byte squadIndex;
                if (btnClicked.Name == "_btnAttackerLeft") {
                    squadIndex = 0;
                }
                else if (btnClicked.Name == "_btnAttackerRight") {
                    squadIndex = 1;
                }
                else if (btnClicked.Name == "_btnDefenderLeft") {
                    squadIndex = 2;
                }
                else if (btnClicked.Name == "_btnDefenderRight") {
                    squadIndex = 3;
                }
                else{
                    squadIndex = 4;
                }
                _currentAccount.Squad[squadIndex] = _playerChosen.GetPlayerIdentifier;
                _playerChosen = null;
                return;
            }
            MessageDialog msg = new MessageDialog("Please Select a Player to add");
            await msg.ShowAsync();

        }
    }
    
}
