﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App5
{
    class GameEngine
    {
        private List<Player> _firstTeam;
        private List<Player> _secondTeam;
        private byte score1;
        private byte score2;
        private Random _randomizer;
        public GameEngine(List<Player> firstTeam, List<Player> secondTeam) {
            _firstTeam = firstTeam;
            _secondTeam = secondTeam;
            score1 = 0;
            score2 = 0;
            _randomizer = new Random();
        }
        public string Simulate() {

            score1 += Occassion(_firstTeam[0],_secondTeam);
            score1 += Occassion(_firstTeam[0], _secondTeam);
            score1 += Occassion(_firstTeam[1], _secondTeam);
            score1 += Occassion(_firstTeam[1], _secondTeam);
            score2 += Occassion(_secondTeam[0], _firstTeam);
            score2 += Occassion(_secondTeam[0], _firstTeam);
            score2 += Occassion(_secondTeam[1], _firstTeam);
            score2 += Occassion(_secondTeam[1], _firstTeam);
            if (score1 ==0 && score2 ==0) {
                if (_randomizer.Next(1,100)>= 50) {
                    return "You Have Won The Flip";
                }
                return "Team 2 Has Won The Flip";
            }
            if (score1 > score2) {
                return "You Have Won";
            }
            if (score1 == score2) {
                return "Tie";
            }
            return "Team 2 Has Won";
        }

        private byte Occassion(Player Attacker, List<Player> Defenders) {
            int total = 0;
            total += Attacker.GetPlayerStats[3] - Defenders[2].GetPlayerStats[5];
            total += Attacker.GetPlayerStats[4] - Defenders[2].GetPlayerStats[4];
            total += Attacker.GetPlayerStats[0] - Defenders[2].GetPlayerStats[0];
            if (total < 20) {
                total = 20;
            }
            if (_randomizer.Next(1,100)<= total)
            {
                total = 0;
                total += Attacker.GetPlayerStats[3] - Defenders[3].GetPlayerStats[5];
                total += Attacker.GetPlayerStats[4] - Defenders[3].GetPlayerStats[4];
                total += Attacker.GetPlayerStats[0] - Defenders[3].GetPlayerStats[0];
                if (total < 20)
                {
                    total = 20;
                }
                if (_randomizer.Next(1, 100) <= total) {

                    total = 0;
                    total += Attacker.GetPlayerStats[3] - Defenders[4].GetPlayerStats[4];
                    if (_randomizer.Next(1,100) <= Attacker.GetPlayerStats[7]) {
                        //Stat
                        total += Attacker.GetPlayerStats[7] - Defenders[4].GetPlayerStats[0];
                        total += Attacker.GetPlayerStats[1] - Defenders[4].GetPlayerStats[3];
                        total += Attacker.GetPlayerStats[6] - Defenders[4].GetPlayerStats[1];
                        if (_randomizer.Next(1,100) <= total) {
                            //GOOOOAAALLLLL
                            return 1;
                        }
                    }
                    return 0;

                }
                return 0;

            }
            return 0;

        }
    }
}
