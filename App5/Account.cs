﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App5
{


    public class Account
    {
        #region Field Variables
        private int _accountRecordIndentifier;
        private string _username;
        private string _name;
        private string _email;
        private string _password;
        private string _teamname;
        private int _gameRecordPointer;
        private string[] _squadPlayers;
        #endregion
        #region Properties
        public int AccountRecordIdentifier {
            get { return _accountRecordIndentifier; }
        }
        public string Username {
            get { return _username; }
        }
        public string Name {
            get { return _name; }
        }
        public string Email {
            get { return _email; }
        }
        public string Password {
            get { return _password; }
        }
        public string TeamName {
            get { return _teamname; }
        }
        public int RecordPointerIdentifier {
            get { return _gameRecordPointer; }
        }
        public string[] Squad {
            get { return _squadPlayers; }
            set { _squadPlayers = value; }
        }

        #endregion
        public Account(int accountRecordIdentifier, string username, string name, string email, string password, string teamname, string squadPlayers)
        {
            _accountRecordIndentifier = accountRecordIdentifier;
            _username = username;
            _name = name;
            _email = email;
            _password = password;
            //_gameRecordPointer = gameRecordPointer;
            _squadPlayers = squadPlayers.Split(',');
            _teamname = teamname;
        }
        internal string squadLine() {

            string squadString = "";
            if (_squadPlayers.Length != 0)
            {


                for (byte i = 0; i < _squadPlayers.Length - 1; i++)
                {
                    squadString += $"{_squadPlayers[i]},";
                }
                squadString += $"{_squadPlayers[_squadPlayers.Length - 1]}";
                return squadString;
            }
            return "0,0,0,0,0";
        }
        internal string writeProperty()
        { 
            return $"\n{_accountRecordIndentifier}|{_username}|{_name}|{_email}|{_password}|{_teamname}|{squadLine()}\n";
        }
    }
}
