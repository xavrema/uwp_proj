﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App5
{
    internal class Player
    {
        private string _identifier;
        private string _name;
        private string _description;
        private string _number;
        private string _country;
        private string _position;
        private byte[] _stats;
        public string GetPlayerIdentifier {
            get { return _identifier; }
        }
        public string GetPlayerCountry{
                get{return _country;}
        }
        public byte[] GetPlayerStats {
            get { return _stats; }
        }
        public string GetPlayerName {
            get { return _name; }
        }
        public string GetPlayerNumber {
            get { return _number; }
        }
        public string GetPlayerDescription {
            get { return _description; }
        }

        public string GetPlayerPosition
        {
            get { return _position; }
        }


        public Player(string allStats) {
            // 100 | DeGea | The heir to Casillas's throne|13|Spain|Goalkeeper|88,85,87,90,85
            string[] indiPlayer = allStats.Split('|');
            _identifier = indiPlayer[0];
            _name = indiPlayer[1];
            _description = indiPlayer[2];
            _number = indiPlayer[3];
            _country = indiPlayer[4];
            _position = indiPlayer[5];
            string[] stringStats = indiPlayer[6].Split(',');
            _stats = new byte[stringStats.Length];
            for (byte i =0;i<stringStats.Length;i++) {
                _stats[i] = Convert.ToByte(stringStats[i]);
            }
        }
    }
}