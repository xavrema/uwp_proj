﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace App5
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        #region Field Variables
        private Button _btnCreateAccountcs;
        private Button _btnSignIncs;
        public string[] _txtAccounts;
        private List<string> _validAccount;
        private List<Player> _allPlayers;
        #endregion
        #region Property
        public List<string> ValidAccount {
            get { return _validAccount; }
            set { _validAccount = value; }
        }
        public Button CreateAccountButton
        
        {
            get { return _btnCreateAccountcs; }
            set { _btnCreateAccountcs = value; }
        }
         public Button CreateSignInButton {
            get { return _btnSignIncs; }
            set { _btnSignIncs = value; }
        }
        public string[] ReadAccounts {
            get { return _txtAccounts; }
            set { _txtAccounts = value; }
        }
        #endregion
        public MainPage()
        {
            this.InitializeComponent();
            CallLocalAssets();
            CallPlayerAssets();
        }

        private async void CreateLocalAccount() {
            
        }
        public void SaveButtons() {
            CreateAccountButton = _btnCreateAccount;
            CreateSignInButton = _btnSignIn;
            _spanelDisplay.Children.Clear();
        }

        private void OnClickSignIn(object sender, RoutedEventArgs e)
        {
            SaveButtons();
            CreateTextBox("Username");
            CreateTextBox("Password");
            CreateButton("Submit","_btnSignInSubmit");
            CreateButton("Cancel", "_btnCancel");
        }
        private void CreateTextBox(string PlaceHolderText) {
            TextBox TxtBox = new TextBox
            {
                PlaceholderText = PlaceHolderText,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Margin = new Thickness(0, 0, 0, 30)
            };
            _spanelDisplay.Children.Add(TxtBox);
        }
        private void OnClickCreateAccount(object sender, RoutedEventArgs e)
        {
            SaveButtons();
            CreateTextBox("Username");
            CreateTextBox("Name");
            CreateTextBox("Email");
            CreateTextBox("Password");
            CreateTextBox("Confirm Password");
            CreateButton("Submit","_btnCreateAccountSubmit");
            CreateButton("Cancel", "_btnCancel");

        }
        private void CreateButton(string NameOfText, string NameOfButton) {
            Button btnCreate = new Button()
            {
                Content = NameOfText,
                HorizontalAlignment = HorizontalAlignment.Stretch,
                Name = NameOfButton,
                Margin = new Thickness(0,0,0,20)
                   
                
            };
            if (NameOfText == "Submit")
            {
                btnCreate.Click += new RoutedEventHandler(OnClickSubmit);
            }
            else {
                btnCreate.Click += new RoutedEventHandler(OnClickRefreshPage);
            }
            
            _spanelDisplay.Children.Add(btnCreate);
        }

        
        private void OnClickSubmit(object sender, RoutedEventArgs e)
        {
            ResetErrorBoxes();
            Button clickedButton = (Button)sender;
            bool IfSignIn = false;
            MessageDialog msg;
            List<string> Contents = new List<string>();
            if (clickedButton.Name.ToString() == "_btnSignInSubmit") {
                IfSignIn = true;
            }
            if (IfSignIn)
            {
                TextBox txtbxUsername = (TextBox)_spanelDisplay.Children[0];
                TextBox txtbxPassword = (TextBox)_spanelDisplay.Children[1];
                Contents.Add(txtbxUsername.Text);
                Contents.Add(txtbxPassword.Text);
                if (ValidateSignIn(Contents))
                {
                    msg = new MessageDialog("Successfully Signed In");
                    Account newAccount = CreateAccountObj(ValidAccount);
                    this.Frame.Navigate(typeof(BlankPage1), new PassedParameters(newAccount,_txtAccounts, _allPlayers));
                }
                else {
                    msg = new MessageDialog("Failed to Sign In");
                }

            }
            else {
                for(int i=0; i< _spanelDisplay.Children.Count -2; i++) {
                    TextBox txtbxUserInfo = (TextBox)_spanelDisplay.Children[i];
                    Contents.Add(txtbxUserInfo.Text);
                }
                if (ValidateCreateAccount(Contents))
                {
                    StackPanel Container = new StackPanel();
                    TextBlock EnterNameLabel = new TextBlock
                    {
                        Text = "Teamname",
                        FontSize = 36
                    };
                    TextBox EnterName = new TextBox
                    {
                        PlaceholderText = "Enter Teamname",
                        FontSize = 36,
                        Name = "_txtbxTeamName"
                    };
                    Button teamNameSender = new Button {
                        Content = "Submit",
                        FontSize = 36
                    };
                    teamNameSender.Click += new RoutedEventHandler(ValidTeamName);
                    Container.Children.Add(EnterNameLabel);
                    Container.Children.Add(EnterName);
                    Container.Children.Add(teamNameSender);

                    Flyout EnterInfo = new Flyout
                    {

                    };
                    EnterInfo.Content = Container;
                    EnterInfo.ShowAt(_spanelDisplay as FrameworkElement);
                }
                else {
                    msg = new MessageDialog("Highlighted Fields Are Incorrect");
                }

            }
            //msg.ShowAsync();
            
            //RefreshPage();
        }

        private void ValidTeamName(object sender, RoutedEventArgs e)
        {
            var sentButton = sender as Button;
            var SpanelParent = sentButton.Parent as StackPanel;
            var ContentText = SpanelParent.Children[1] as TextBox;
            bool Exist = false;
            int counter = 0;
            while (counter != (_txtAccounts.Count()) && !Exist) {
                string accountLine = _txtAccounts[counter];
                string[] indiAccount = accountLine.Split('|');
                if (indiAccount[5] == ContentText.Text)
                {
                    //stop loop and display name already taken please enter a new name
                    Exist = true;
                    _validAccount.Add(ContentText.Text);
                    

                }
                counter += 1;
            }
            if (Exist)
            {
                ContentText.BorderBrush = new SolidColorBrush(Colors.Red);
                TextBlock errorName = new TextBlock()
                {
                    Text = "Invalid Name",
                    Foreground = new SolidColorBrush(Colors.Red)
                };
                SpanelParent.Children.Add(errorName);
            }
            else {
                Account newAccount = CreateAccountObj(_validAccount);
                SaveAccount(newAccount);
                
                this.Frame.Navigate(typeof(BlankPage2), new PassedParameters(newAccount, _txtAccounts, _allPlayers));
            }



        }

        private async void SaveAccount(Account newAccount)
        {
            Windows.Storage.StorageFolder storageFolder = Windows.Storage.ApplicationData.Current.LocalFolder;
            Windows.Storage.StorageFile currentFile = await storageFolder.CreateFileAsync( "newAccounts.dat", CreationCollisionOption.OpenIfExists);
            await Windows.Storage.FileIO.AppendTextAsync(currentFile, newAccount.writeProperty());

        }

        private void ResetErrorBoxes()
        {
            int spanelChildren = _spanelDisplay.Children.Count;
            for (byte i =0; i<spanelChildren-3;i++) {
                var ele = _spanelDisplay.Children[i] as TextBox;
                ele.BorderBrush = new SolidColorBrush(Colors.DarkGray );
                

            }

        }

        private void OnClickRefreshPage(object sender, RoutedEventArgs e) {
            _spanelDisplay.Children.Clear();
            _spanelDisplay.Children.Add(_btnSignIncs);
            _spanelDisplay.Children.Add(_btnCreateAccountcs);
        }
        private bool ValidateSignIn(List<string> Contents ) {
            string[] allAccount = ReadAccounts;
            bool popupVal = false;
            foreach (string Account in allAccount)
            {

                string[] indiAccount = Account.Split('|');
                
                bool equal = Contents[0] == indiAccount[1];
                if (equal)
                {
                    if (Contents[1] == indiAccount[4])
                    {
                        
                        ValidAccount = indiAccount.OfType<string>().ToList(); ;
                        return true;
                    }
                }
            }
            return popupVal;
        }
        private bool ValidateCreateAccount(List<string> Contents) {
            bool popupVal = true;


            foreach (string Account in ReadAccounts)
            {
                //Username,Name,Email,Password,PasswordVal
                //Account Identifier,Username,Name,Email,Password
                string[] indiAccount = Account.Split('|');

                if (Contents[0] == indiAccount[1] || Contents[0].ToString() == "")
                {
                    TextBox username = _spanelDisplay.Children[0] as TextBox;
                    username.BorderBrush = new SolidColorBrush(Colors.Red);
                    popupVal = false;
                }
                if (Contents[1].ToString() == "") {
                    TextBox name = _spanelDisplay.Children[1] as TextBox;
                    name.BorderBrush = new SolidColorBrush(Colors.Red);
                    popupVal = false;
                }
                if (Contents[2] == indiAccount[3] || (Contents[2].IndexOf('@') == -1))
                {
                    TextBox email = _spanelDisplay.Children[2] as TextBox;
                    email.BorderBrush = new SolidColorBrush(Colors.Red);
                    popupVal = false;
                }
                if (Contents[3].ToString() == "" || Contents[4].ToString() == "" || Contents[3] != Contents[4])
                {
                    TextBox password1 = _spanelDisplay.Children[3] as TextBox;
                    TextBox password2 = _spanelDisplay.Children[4] as TextBox;
                    password1.BorderBrush = new SolidColorBrush(Colors.Red);
                    password2.BorderBrush = new SolidColorBrush(Colors.Red);
                    popupVal = false;
                }
            }
            if (popupVal) {
                ValidAccount = Contents;

            }
            return popupVal;
        }
        private async void CallLocalAssets()
        {
            StorageFile accountList = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(@"Assets\Data\Accounts.txt");
            Stream fileStream = await accountList.OpenStreamForReadAsync();
            var buffer = await Windows.Storage.FileIO.ReadBufferAsync(accountList);
            using (var dataReader = DataReader.FromBuffer(buffer))
            {
                //Length is every character
                string text = dataReader.ReadString(buffer.Length);
                ReadAccounts = text.Split('`');
            }
        }
        private async void CallPlayerAssets(){ 
                    StorageFile playerList = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(@"Assets\Data\Players.txt");

            var playerBuffer = await Windows.Storage.FileIO.ReadBufferAsync(playerList);
                    using (var dataReaderPlayer = DataReader.FromBuffer(playerBuffer))
                    {
                        //Length is every character
                        string Playertext = dataReaderPlayer.ReadString(playerBuffer.Length);
                        string[] ListPlayers = Playertext.Split('\n');
                        List<string> Countries = new List<string>();
                        _allPlayers = new List<Player>();
                        for (byte i = 4; i < ListPlayers.Length; i++)
                        {
                            Player indiPlayer = new Player(ListPlayers[i]);
                            _allPlayers.Add(indiPlayer);
                        }
                    }
                
        }
        private Account CreateAccountObj(List<string> Contents) {
            Account newAccount;
            //Username,Name,Email,Password,PasswordVal
            if (int.TryParse(Contents[0], out int RecordIdentifier))
            {
                newAccount = new Account(RecordIdentifier, Contents[1], Contents[2], Contents[3], Contents[4], Contents[5], Contents[6]);

            }
            else
            {
                newAccount = new Account(ReadAccounts.Length, Contents[0], Contents[1], Contents[2], Contents[3], Contents[4], "000,000,000,000,000");

            }

            return newAccount;
        }
                    
    }
}
